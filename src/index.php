<!--
#
#                     Made by Th3Void
#                       Aug 05 2021
#       This is a free and open source software 
#
#
#-->

    <?php include_once 'header.php'?>

    <!-- grid -->
    <div class="row">
        <div class="col-2">
            <?php include_once 'sidebar.php'?>
        </div>
        <div class="col-10">
            <div class="container content">
                <?php include_once 'home.php'?>
            </div>
        </div>
    </div>

    <!-- footer -->
    <?php include_once 'footer.php'?>
