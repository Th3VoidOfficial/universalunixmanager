<footer>
    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
        © <?php echo date("Y")?> Copyright:
        <a class="text-reset fw-bold" href="https://gitlab.com/Th3VoidOfficial/universalunixmanager">Universal Unix Manager by Th3Void</a>
    </div>
    <!-- Copyright -->
</footer>        
</body>
</html>