<!--
#
#                     Made by Th3Void
#                       Aug 05 2021
#       This is a free and open source software 
#
#
#-->

<?php 

    include_once 'mono.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="static/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/css/custom.css">
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/popper.min.js"></script>
    <link rel="shortcut icon" href="static/data/cat.png" type="image/x-icon">
    <title>Universal Unix Manager</title>
</head>
<body>    
    <header>
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Universal Unix Manager (UwU)</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarID"
                    aria-controls="navbarID" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
               
            </div>
        </nav>
    </header>