<?php 

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

function sys($command) {
	system($command);	
}

function main(){

	echo("<hr>");
	sys('w');

	echo("<hr>");
	sys('lspci');

	echo("<hr>");
	sys('lsusb');

	echo("<hr>");
	sys('ifconfig');

}

function get_server_cpu_usage(){

	$load = sys_getloadavg();
	return $load[0];

}

function get_server_memory_usage(){
	
	$free = shell_exec('free');
	$free = (string)trim($free);
	$free_arr = explode("\n", $free);
	$mem = explode(" ", $free_arr[1]);
	$mem = array_filter($mem);
	$mem = array_merge($mem);
	$memory_usage = $mem[2]/$mem[1]*100;

	return $memory_usage;
}


function ret_load() {

	echo "<p><span class='description'>Server Memory Usage:</span> <span class='result'>";
	echo get_server_memory_usage();
	echo "</span></p>";

	echo "<p><span class='description'>Server CPU Usage: </span> <span class='result'>"; 
	echo get_server_cpu_usage(); 
	echo "</span></p>";

}

function loader($opt) {
	switch ($opt) {
		case '1':
			main();
			break;
		case '2':
			ret_load();
			break;
		
		case '1':
			main();
			break;
		case '1':
			main();
			break;
		default:
			# code...
			break;
	}
}

?>
